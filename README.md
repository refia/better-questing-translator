**Как использовать**
---
**Конечным пользователям:**
Скачать архив [от сюда] (нажать на "Download repository")---> 
распаковать, открыть index.html ---> 
в первый фаил инпут закинуть DefaultQuests.json ---> 
во второй - json конфиг с переводом, затем подождать пару сек, пока осуществляется перевод. ---> 
жмякнуть по "**Export Translated Quests**" - скачается новый, русифицированный DefaultQuests.json
---
*for Developers (translators):*

1. Download .zip [from here]
2. Unpack and launch index.html
3. select DefaultQuests.json to first file input
4. Make (partial) translation.
5. Button "**auto**" allow you to make auto translate for a quest line by yandex translate.
6. After you made some translation, you may save your partial result as json file, that may be used to continue translation later.
7. Don't forget to mark translated lines as "**translated**", to include them to generated translation json.
8. To continue translation, in additionally with **DefaultQuests.json** you have to select your **translateBqIia.json** to the second file input.
9. Finally, after you completely finish (or early, just for test) you may export your new translated quests config - **DefaultQuests.json** by pressing on "**Export Translated Quests**"

[from here]: https://bitbucket.org/refia/better-questing-translator/downloads/
[от сюда]: https://bitbucket.org/refia/better-questing-translator/downloads/