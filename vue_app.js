window.addEventListener("load", function (event) {

    let app = new Vue({
        el: '#app',
        data: {
            version: '1.2',
            keys: {
                db: 'questDatabase:9',
                prop: 'properties:10',
                bq: 'betterquesting:10',
                qName: 'name:8',
                qDesc: 'desc:8',
                qs: 'questLines:9'
            },
            questsCount: 0,
            futureSearchText: '',
            futureLangSearchText: '',
            searchText: '',
            langSearchText: '',
            langLines: [],
            lineItemsPerPage: 20,
            questItemsPerPage: 10,
            pages: 1,
            currentPage: 1,
            currentMode: 'quests',
            isQuestsSetView: true,
            trTargets: ['quests','.lang','debug'],
            translatedCount: 0,
            totalLines: 0,
            parsedLines: 0,
            listToRender: [],
            linesQueue: [],
            currentPageLines: [],
            parsingInProgress: false,
            currentView: 'all',
            currentLangView: 'all',
            viewValues: ['all', 'changed', 'in progress', 'translated', 'not translated', 'with empty orig desc'],
            langViewValues: ['all', 'with empty translation'],
            bqData: null,
            totalFilesToReadAndParse: 0,
            filesParsingData: [],
            searchTimeoutId: null,
            translateData: [],
            translateSetData: [],
            translateExist: null,
            settings: {visible: 'all'},
            origFileName: null,
        },
        mounted: function(){
        },
        methods: {
            readBqFile: function (ev) {
                readData(ev, (fileText) => {
                    this.currentPage = 1;
                    this.bqData = JSON.parse(fileText);
                    if(this.currentMode == 'debug'){
                        window.fileJson = Object.assign({},this.bqData);
                        return;
                    }
                    let questKeys = Object.keys(this.bqData[this.keys.db]);
                    let questSetKeys = Object.keys(this.bqData[this.keys.qs]);
                    this.questsCount = questKeys.length;
                    questKeys.forEach(qk => {
                        this.getNameAndDescription(qk, false,  (name, desc) => {
                            name = name.trim().replace(/\s+/g, ' ');
                            desc = desc.trim().replace(/\s+/g, ' ');
                            this.translateData.push({isDone: false, qId: qk, orig:{name, desc},
                                translated: {name: '', desc: ''}});
                        });
                    });
                    questSetKeys.forEach(qsk => {
                        this.getNameAndDescription(qsk, true,  (name, desc) => {
                            name = name.trim().replace(/\s+/g, ' ');
                            desc = desc.trim().replace(/\s+/g, ' ');
                            this.translateSetData.push({isDone: false, qId: qsk, orig:{name, desc},
                                translated: {name: '', desc: ''}});
                        });
                    });
                    console.log('this.translateData:',this.translateData);
                    console.log('this.translateSetData:',this.translateSetData);
                    this.updateViewList();
                });
            },
            checkNoRewards(){
                let qList = Object.keys(window.fileJson['questDatabase:9']);
                let cnt = 0;
                let questNames = [];
                qList.forEach(k => {
                    if(Object.keys(window.fileJson['questDatabase:9'][k]['rewards:9']) == 0){
                        cnt++;
                        questNames.push(window.fileJson['questDatabase:9'][k]['properties:10']['betterquesting:10']['name:8']);
                    }
                });
                console.log('cnt: ', cnt);
                console.log('questNames (without rewards): ', questNames);
            },
            cutMs(){
                let qList = Object.keys(window.fileJson['questDatabase:9']);
                let cnt = 0;
                qList.forEach(k => {
                    let desc = window.fileJson['questDatabase:9'][k]['properties:10']['betterquesting:10']['desc:8'];
                    if(/\s{2,}/.test(desc)){
                        cnt++;
                        window.fileJson['questDatabase:9'][k]['properties:10']['betterquesting:10']['desc:8'] = desc.replace(/\s+/g, ' ');
                    }
                });
                console.log('cnt: ', cnt);
            },
            generateChangedQuests(){
                downloadAsFile(JSON.stringify(window.fileJson), 'json', 'DefaultQuests.json');
            },
            generateTranslatedLandFile(){
                let fileText = '';
                this.langLines.forEach(line => {
                    fileText += line.label+'='+(line.translated || '')+'\n';
                });
                downloadAsFile(fileText, 'lang', this.origFileName);
            },
            getProgressPercent(){
                this.filesParsingData[this.filesParsingData.length-1].progress =
                    (this.parsedLines && this.totalLines) ?
                    (Math.round((this.parsedLines / this.totalLines) * 100)) : '';
                    return this.filesParsingData[this.filesParsingData.length-1].progress;
            },
            readLangFile: function(ev, isOrig){
                if(!this.origFileName && ev.target.files[0]) {
                    this.origFileName = ev.target.files[0].name;
                }
                let isLine = /\=/;
                let linesToTranslate = [];
                let onStackReadFinish = () => {
                    this.langLines = this.langLines.concat(linesToTranslate);
                    this.pages = Math.floor(this.langLines.length / this.lineItemsPerPage) + 1;
                };
                readData(ev, (fileText) => {
                    this.currentPageLines = null;
                    this.currentPage = 1;
                    this.totalFilesToReadAndParse++;
                    let lines = fileText.split('\n');
                    if(!isOrig && this.parsingInProgress){
                        this.linesQueue.push(lines);
                        return;
                    }
                    this.totalLines = lines.length;
                    this.parsedLines = 0;
                    this.parsingInProgress = true;
                    let readNextLine = (linesArray, isOrigFile, onDone) => {
                        if (linesArray.length == 0) {
                            onDone();
                        } else {
                            let l = linesArray.splice(0,1)[0].trim();
                            if(isLine.test(l)){
                                let sliceIdx = l.indexOf('=');
                                let label = l.slice(0,sliceIdx);
                                let text = l.slice(sliceIdx+1);
                                if(isOrigFile){
                                    linesToTranslate.push({
                                        label: label,
                                        orig: text,
                                        translated: ''
                                    });
                                    if (linesToTranslate.length >= this.lineItemsPerPage) {
                                        this.langLines = this.langLines.concat(linesToTranslate);
                                        linesToTranslate = [];
                                        this.pages = Math.floor(this.langLines.length / this.lineItemsPerPage) + 1;
                                        if (!this.currentPageLines) {
                                            this.changePage(0);
                                        }
                                    }
                                } else {
                                    this.langLines.forEach(line => {
                                        if(line.label == label) line.translated = text;
                                    });
                                }
                            }
                            setTimeout(()=>{
                                if(window.isTest && linesToTranslate.length > 280){
                                    readNextLine([], isOrigFile, onDone);
                                } else {
                                    readNextLine(linesArray, isOrigFile, onDone);
                                }
                            },1);
                        }
                        this.parsedLines++;
                    };
                    readNextLine(lines, isOrig, () => {
                        // if(isOrig) this.langLines = linesToTranslate;
                        if(this.linesQueue.length > 0){
                            this.filesParsingData.push({progress: 0});
                            let otherLines = this.linesQueue.splice(0,1)[0];
                            this.totalLines = otherLines.length;
                            this.parsedLines = 0;
                            this.parsingInProgress = true;
                            readNextLine(otherLines, false, () => {
                                onStackReadFinish();
                                this.parsingInProgress = false;
                                // playNotificationSound();
                            });
                        } else {
                            onStackReadFinish();
                            this.parsingInProgress = false;
                        }
                    });
                    this.filesParsingData.push({progress: 0});
                });
            },
            changeQuestView: function(trMapItems){
                this.isQuestsSetView = !this.isQuestsSetView;
                this.currentPage = 1;
                this.updateViewList();
            },
            updateViewList: function() {
                if (this.currentMode == '.lang') {
                    this.currentPageLines = this.langLines.slice(
                        (this.currentPage - 1) * this.lineItemsPerPage,
                        this.currentPage * this.lineItemsPerPage
                    );
                } else if (this.currentMode == 'quests') {
                    this.listToRender = [];
                    this.listToRender = this.filterByView(this.isQuestsSetView ? this.translateSetData : this.translateData);
                }
            },
            filterByView: function(trMapItems) {
                let arrToRender = [];
                if(this.currentView == 'all') {
                    arrToRender = trMapItems;
                } else {
                    trMapItems.forEach(item => {
                        if(this.currentView == 'changed' && item.old) arrToRender.push(item);
                        if(this.currentView == 'in progress' && !item.isDone &&
                            (item.translated.name || item.translated.desc)) {
                            arrToRender.push(item);
                        }
                        if(this.currentView == 'translated' && item.isDone) arrToRender.push(item);
                        if(this.currentView == 'not translated' && !item.isDone) arrToRender.push(item);
                        if(this.currentView == 'with empty orig desc' && item.orig.name && !item.orig.desc) arrToRender.push(item);
                    });
                }
                arrToRender = this.applyTextSearch(arrToRender);
                this.pages = Math.floor(arrToRender.length / this.questItemsPerPage)+1;
                return arrToRender.slice(
                    (this.currentPage - 1) * this.questItemsPerPage,
                    this.currentPage * this.questItemsPerPage
                );
            },
            applyTextSearch: function(arrToRender){
                let secondTierFilter = [];
                if(!this.searchText) return arrToRender;
                else {
                    arrToRender.forEach(item => {
                        if(isStrContain(item.orig.name, this.searchText) ||
                            isStrContain(item.orig.desc, this.searchText) ||
                            isStrContain(item.translated.name, this.searchText) ||
                            isStrContain(item.translated.desc, this.searchText)){
                            secondTierFilter.push(item);
                        }
                    });
                    return secondTierFilter;
                }
            },
            doSearch: function(){
                if(this.searchTimeoutId) clearTimeout(this.searchTimeoutId);
                this.searchTimeoutId = setTimeout(() => {
                    this.currentPage = 1;
                    this.searchText = this.futureSearchText;
                    this.updateViewList();
                }, 800);
            },
            applySearch: function(allLines){
                document.getElementsByTagName('body')[0].style.cursor = 'progress';
                this.currentPageLines = [];
                let listToShow = [];
                if(!this.langSearchText) {
                    allLines.forEach(line => {
                        if (this.currentLangView == 'all' ||
                            (this.currentLangView == 'with empty translation' && !line.translated)) {
                            listToShow.push(line);
                        }
                    });
                }
                else {
                    allLines.forEach(line => {
                        if( isStrContain(line.label, this.langSearchText) ||
                            isStrContain(line.orig, this.langSearchText) ||
                            isStrContain(line.translated, this.langSearchText)) {
                            if(this.currentLangView == 'all' ||
                                (this.currentLangView == 'with empty translation' && !line.translated)) {
                                listToShow.push(line);
                            }
                        }
                    });
                }
                document.getElementsByTagName('body')[0].style.cursor = 'default';
                this.pages = Math.floor(listToShow.length / this.lineItemsPerPage)+1;
                this.onPageChange();
                this.currentPageLines = listToShow.slice(
                    (this.currentPage - 1) * this.lineItemsPerPage,
                    this.currentPage * this.lineItemsPerPage
                );
            },
            getNameAndDescription: function (qk, isQuestSet, callback) {
                if(this.bqData[isQuestSet ? this.keys.qs : this.keys.db] &&
                    this.bqData[isQuestSet ? this.keys.qs : this.keys.db][qk] &&
                    this.bqData[isQuestSet ? this.keys.qs : this.keys.db][qk][this.keys.prop] &&
                    this.bqData[isQuestSet ? this.keys.qs : this.keys.db][qk][this.keys.prop][this.keys.bq] &&
                    this.bqData[isQuestSet ? this.keys.qs : this.keys.db][qk][this.keys.prop][this.keys.bq][this.keys.qName] &&
                    this.bqData[isQuestSet ? this.keys.qs : this.keys.db][qk][this.keys.prop][this.keys.bq][this.keys.qDesc]){
                    callback(this.bqData[isQuestSet ? this.keys.qs : this.keys.db][qk][this.keys.prop][this.keys.bq][this.keys.qName],
                        this.bqData[isQuestSet ? this.keys.qs : this.keys.db][qk][this.keys.prop][this.keys.bq][this.keys.qDesc]);
                }
            },
            changePage: function(n){
                this.currentPage += Number(n);
                if(this.currentPage < 1) this.currentPage = 1;
                else if(this.currentPage > this.pages) this.currentPage = this.pages;
                this.updateViewList();
            },
            onPageChange: function(){
                if(isNaN(this.currentPage)) this.currentPage = 1;
                this.currentPage = Number(this.currentPage);
                if(this.currentPage < 1) this.currentPage = 1;
                else if(this.currentPage > this.pages) this.currentPage = this.pages;
                this.updateViewList();
            },
            doLangSearch(){
                this.langSearchText = this.futureLangSearchText;
                this.applySearch(this.langLines);
            },
            readTranslateJson: function (ev) {
                readMultiJson(ev, (jsons) => {
                    jsons.forEach(jsConfig => {
                        if(Array.isArray(jsConfig)){
                            jsConfig = {quests: jsConfig, sets: []};
                        }
                        this.translateData.forEach(t => {
                            jsConfig.quests.forEach(p => {
                                if(t.qId == p.qId){
                                    if(t.orig.name == p.orig.name && t.orig.desc == p.orig.desc){
                                        t.translated = p.translated;
                                        t.isDone = true;
                                    } else {
                                        t.old = p;
                                    }
                                }
                            });
                        });
                        this.translateSetData.forEach(t => {
                            jsConfig.sets.forEach(p => {
                                if(t.qId == p.qId){
                                    if(t.orig.name == p.orig.name && t.orig.desc == p.orig.desc){
                                        t.translated = p.translated;
                                        t.isDone = true;
                                    } else {
                                        t.old = p;
                                    }
                                }
                            });
                        });
                    });
                });
            },
            isAllQuestsWithTextMarkedAsTranslated: function(){
                let foundTranslatedButNotMarkedQuest = false;
                this.translateData.forEach(item => {
                    if(!item.isDone && (item.translated.name || item.translated.desc))
                        foundTranslatedButNotMarkedQuest = true;
                });
                this.translateSetData.forEach(item => {
                    if(!item.isDone && (item.translated.name || item.translated.desc))
                        foundTranslatedButNotMarkedQuest = true;
                });
                return !foundTranslatedButNotMarkedQuest;
            },
            isAnyQuestsWithOldTranslate: function(){
                let foundOldTranslateQuest = false;
                this.translateData.forEach(item => {
                    if(item.old)
                        foundOldTranslateQuest = true;
                });
                this.translateSetData.forEach(item => {
                    if(item.old)
                        foundOldTranslateQuest = true;
                });
                return foundOldTranslateQuest;
            },
            generateTranslatedQuests: function () {
                if(!this.isAllQuestsWithTextMarkedAsTranslated() &&
                    !confirm('WARNING! Some quests (or quest sets) are translated, but not marked as translated.You may see them by filter ' +
                        '"in progress". Do you want to skip them?')) return;
                if(this.isAnyQuestsWithOldTranslate() &&
                    !confirm('WARNING! Some quests (or quest sets) have old translate, and them need to be refreshed.' +
                        'You may see them by filter "changed". Do you want to skip them?')) return;
                let questKeys = Object.keys(this.bqData[this.keys.db]);
                let trMap = {};
                this.translateData.forEach(item => {
                    trMap[item.qId] = item;
                });
                questKeys.forEach(qk => {
                    if(trMap[qk] && trMap[qk].isDone && trMap[qk].translated && trMap[qk].translated.name){
                        this.bqData[this.keys.db][qk][this.keys.prop][this.keys.bq][this.keys.qName] = trMap[qk].translated.name;
                    }
                    if(trMap[qk] && trMap[qk].isDone && trMap[qk].translated && trMap[qk].translated.desc){
                        this.bqData[this.keys.db][qk][this.keys.prop][this.keys.bq][this.keys.qDesc] = trMap[qk].translated.desc;
                    }
                });
                downloadAsFile(JSON.stringify(this.bqData), 'json', 'DefaultQuests.json');
            },
            getCurrentTranslateConfig: function () {
                if(!this.isAllQuestsWithTextMarkedAsTranslated() &&
                    !confirm('WARNING! Some quests (or quest sets) are translated, but not marked as translated.You may see them by filter ' +
                        '"in progress". Do you want to skip them?')) return;
                if(this.isAnyQuestsWithOldTranslate() &&
                    !confirm('WARNING! Some quests (or quest sets) have old translate, and them need to be refreshed.' +
                        'You may see them by filter "changed". Do you want to skip them?')) return;
                let questLinesToSave = [];
                let setLinesToSave = [];
                this.translatedCount = 0;
                this.translateData.forEach(t => {
                    if(t.isDone) {
                        questLinesToSave.push(t);
                        this.translatedCount++;
                    }
                });
                this.translateSetData.forEach(t => {
                    if(t.isDone) {
                        setLinesToSave.push(t);
                    }
                });
                downloadAsFile(JSON.stringify({quests: questLinesToSave, sets: setLinesToSave}), 'json', `translateBqIIA-${this.translatedCount}-of-${this.questsCount}.json`);
            }
        }
    });

});