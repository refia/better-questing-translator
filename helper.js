
function isSame(str1, str2) {
    return String(str1).trim().toLowerCase() == String(str2).trim().toLowerCase();
}

function isStrContain(full, part) {
    full = String(full).trim().toLowerCase().replace(/\s+/g, ' ');
    part = String(part).trim().toLowerCase().replace(/\s+/g, ' ');
    return full.indexOf(part) > -1;
}

function playNotificationSound(){
    let mp3Source = '<source src="eventually.mp3" type="audio/mpeg">';
    document.getElementById("sound").innerHTML='<audio onplay="setVolume(this, 0.05)" autoplay="autoplay">' + mp3Source + '</audio>';
}

function setVolume(el, val) {
    el.volume = val;
    setTimeout(()=>{
        el.parentNode.removeChild(el);
    },3000);
}

function downloadAsFile(data, type, fileName) {
    let file = new Blob([data], {type: (type || 'data:text/plain;charset=utf-8')});
    let a = document.createElement("a");
    let url = URL.createObjectURL(file);
    a.href = url;
    a.download = fileName;
    a.text = 'get new config';
    document.body.appendChild(a);
    a.style.display = 'none';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}

function readData(ev, callback) {
    let file = ev.target.files[0];
    let fr = new FileReader();
    fr.onload = function (res) {
        callback(res.target.result);
    };
    fr.readAsText(file);
}
function readMultiJson(ev, callback) {
    let jsons = [];
    let filesToRead = ev.target.files.length;
    let readNext = (files, filesLeft)=>{
        if(filesLeft == 0){
            callback(jsons);
        } else {
            let file = ev.target.files[filesLeft-1];
            let fr = new FileReader();
            fr.onload = function (res) {
                jsons.push(JSON.parse(res.target.result));
                filesLeft--;
                readNext(ev.target.files, filesLeft);
            };
            fr.readAsText(file);
        }
    };
    readNext(ev.target.files, filesToRead);
}

function makeRequest(url, callback) {
    fetch(url)
        .then((response) => {
            if(response.ok) {
                return response.json();
            } else throw new Error('Network response was not ok');
        })
        .then((json) => {
            callback(json);
        })
        .catch((error) => {
            console.log(error);
        });
}